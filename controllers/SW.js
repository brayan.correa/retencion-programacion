/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var models = require('./../model');
var Retencion = models.retencion;
var Factura = models.factura;
class Sw {
    obtenerProvincias(req, res) {
        var clasificacion = {"clasificacion": ["Profesionales","Eductivos"]};
        res.status(200).json(clasificacion);
    }
    obtenerCantones(req, res) {
        var porcentajes = req.query.porcentajes;
        var valor = {"Profesionales": ["10"],
            "Eductivos": ["8"]};
        res.status(200).json(valor[porcentajes]);
    }
    obtenerListaTodos(req, res) {
        Retencion.findAll({include:{model: Factura}}).then(function (lista){
            
            res.status(200).json(lista);
        });
    }
//    
       obtenerListaTodos3(req, res) {
        Factura.findAll({include:{model: Retencion}}).then(function (lista){
            res.status(200).json(lista);
        });
    }
//    
    
    obtenerListaTodosLotes(req, res) {
        Factura.findAll( {include:{model: Retencion}}).then(function (lista){
            res.status(200).json(lista);
        });
    }
    
    obtenerCantLotes(req, res) {
        Factura.count({col: "id_retencion"}, {where: {id_retencion: 5}}, {include:{model: Retencion}}).then(function (cont){
            res.status(200).json({cantidad: cont});
        });
    }
    
    obtenerCantProducto(req, res) {
        var idProd  = req.params.codigo;
        Factura.sum("valor", {where: {id_retencion: idProd }}, {include:{model: Retencion}}).then(function (cont) {            
            res.status(200).json({cantidad: cont});
        });
    }
    
    buscarRetencion(req, res){
    var idProd =req.params.buscar;
    Retencion.findAll({where:{fecha_retencion:{"$between":[idProd, idProd]} }, include:{model:Factura}  }).then(function(retencion){
        res.status(200).json(retencion);
    }).catch(function(err){
        res.status(500).json(err);
    });
    }
    
    
}
module.exports = Sw;
