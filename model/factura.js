module.exports = function (sequelize, Sequelize) {    
  
    
    var Factura = sequelize.define('factura', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        }, 
        
       
        numero_factura: {
            type: Sequelize.STRING(50)
        },  
        
        nombre_cliente: {
            type: Sequelize.STRING(50)
        },
        
        cedula: {
            type: Sequelize.STRING(50)
        },
        
        
      
        fecha_factura: {type: Sequelize.DATEONLY} 
        
    }, {timestamps: false,
        freezeTableName: true
    });
    
Factura.associate= function (models){
        models.factura.hasMany(models.retencion, {
            foreignKey:'id_factura'});
    };
    
        
    return Factura;
};

