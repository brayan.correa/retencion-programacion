$(document).ready(function () {
    listarPaises();
});
var url = 'https://restcountries.eu/rest/v2/all';
function listarPaises() {
    $.ajax({
        type: 'GET',
        url: url,
        dateType: 'json',
        success: function (data, textStatus, jqXHR) {
            var html = '';
            $.each(data, function (i, item) {
                html += '<option value="' + item.name + '">' + item.name + '</option>';
            });
            $('#pais').html(html);
            $('#pais_editar').html(html);
        },
        error: function (jqXHR, textStatus, jqXHR) {

        }
    });
}
