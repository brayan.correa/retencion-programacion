var express = require('express');
var router = express.Router();
var sw = require('../controllers/SW');
var SW = new sw();

var prod = require('../controllers/ProductoController');
var Prod = new prod();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('template_admin', { titulo: 'Express', fragmento: "fragmentos/frm_principal_admin"});
});


//TESTS
//Servicio web
router.get('/test/sw/provincias', SW.obtenerProvincias);
router.get('/test/sw/cantones', SW.obtenerCantones);

router.get('/test/sw/listaProdTod', SW.obtenerListaTodos);

router.get('/test/sw/listaProdTod3', SW.obtenerListaTodos3);
router.get('/test/sw/listaProdLot', SW.obtenerListaTodosLotes);
router.get('/test/sw/cantLotes', SW.obtenerCantLotes);
router.get('/test/sw/cantProd/:codigo', SW.obtenerCantProducto);
router.get('/test/sw/:buscar', SW.buscarRetencion);
//retencion
router.get('/test/producto/lista', Prod.listar);
router.get('/test/producto/registrar', Prod.registrar);


router.post('/test/producto/guardar', Prod.guardar);


router.post('/test/producto/editar', Prod.editarREtencion);

//factura
router.get('/test/lote/registrar', Prod.registrarLote);
router.post('/test/lote/guardar', Prod.guardarLote);


module.exports = router;
