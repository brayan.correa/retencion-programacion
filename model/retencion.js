module.exports = function (sequelize, Sequelize) {
          var factura = require('../model/factura');
    var Factura = new factura(sequelize, Sequelize);
    var Retencion = sequelize.define('retencion', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        external_id: {
            type: Sequelize.UUID
        },
        numero_retencion: {
            type: Sequelize.STRING(50)
        },  
        
        valor_factura: {
            type: Sequelize.DOUBLE(7, 2)
        },
        
        valor_retencion: {
            type: Sequelize.DOUBLE(7, 2)
        },
        clasificacion: {type: Sequelize.STRING(50)},
        porsentaje: {type: Sequelize.STRING(50)},
        fecha_retencion: {type: Sequelize.DATEONLY} 
    }, {
        freezeTableName: true
    });
    
    
        Retencion.belongsTo(Factura, {
        foreignKey: 'id_factura',
        constraints: false
    });
    
    
    
    
    return Retencion;
};