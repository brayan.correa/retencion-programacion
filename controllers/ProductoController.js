/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
const uuidv4 = require('uuid/v4');
var models = require('./../model');
var Retencion = models.retencion;
var Factura = models.factura;
class ProductoController {
    listar(req, res) {
        res.render('template_admin',
                {titulo: "Lista de Retencion",
                    fragmento: 'fragmentos/productos/frm_lista'
                });
    }
    registrar(req, res) {
        res.render('template_admin',
                {titulo: "Registro de Retencion",
                    fragmento: 'fragmentos/productos/frm_registro_retencion'
                });
    }
    guardar(req, res) {
        Retencion.create({numero_retencion: req.body.numero_retencion
            , external_id: uuidv4()
            , clasificacion: req.body.clasificacion
            , porsentaje: req.body.porsentaje
            , valor_retencion: req.body.valor_retencion
            , valor_factura: req.body.valor_factura,
            fecha_retencion:req.body.fecha_retencion,
            id_factura: req.body.nombre_cliente
        }).then(function (newProd, created) {
            if (newProd) {
                res.redirect("/test/producto/lista");
            } else {
                res.redirect("/test/producto/registro");
            }
        });
    }
    
    
        editarREtencion(req, res) {
      
        Retencion.update({

            nombre_cliente: req.body.nombre_cliente,
            numero_retencion: req.body.numero_retencion,
            valor_factura: req.body.valor_factura,
            valor_retencion: req.body.valor_retencion,
            clasificacion: req.body.clasificacion,
            porsentaje: req.body.porcentaje,
             id_factura: req.body.nombre_cliente

        }, {where: {external_id: req.body.external}}).then(function (updateRetencion) {
            if (updateRetencion) {
                console.log('+++++++++');
                req.flash('info', 'Se a modificado correctamente');
                res.redirect("/test/producto/lista");
            }
        });
    }
    
    
    
    
    //Factura
    registrarLote(req, res) {
        res.render('template_admin',
                {titulo: "Registro de Factura",
                    fragmento: 'fragmentos/productos/frm_registroLote'
                });
    }
    guardarLote(req, res) {
        Factura.create(
                {
                numero_factura:req.body.numero_factura, 
                cedula: req.body.cedula,
                 
                 
                 fecha_factura: req.body.fi,
                 
                  nombre_cliente: req.body.nombre_cliente
                }
        ).then(function (newLote, created) {
            if (newLote) {
                res.redirect("/test/producto/lista");
            } else {
                res.redirect("/test/lote/registro");
            }
        });
    }
}
module.exports = ProductoController;
